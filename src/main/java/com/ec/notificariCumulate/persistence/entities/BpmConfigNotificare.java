package com.ec.notificariCumulate.persistence.entities;

import org.springframework.data.annotation.Immutable;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "bpm_config_notificare")
@Immutable
public class BpmConfigNotificare {

    @Id
    @Column
    private String cod;

    @Column
    private Long idModul;

    @Column
    private String strTip;

    @Column
    private String aditionalToListaId;

    @Column
    private String aditionalToListaEmail;

    @Column
    private String aditionalCcListaId;

    @Column
    private String aditionalCcListaEmail;

    @Column
    private String exclusToListaId;

    @Column
    private String exclusToListaEmail;

    @Column
    private String exclusCcListaId;

    @Column
    private String exclusCcListaEmail;

    @Column
    private String subiect;

    @Column
    private String antet;

    @Column
    private String continut;

    @Column
    private String subsol;

    @Column
    private Long nrCicluri;

    @Column
    private Boolean areOrar;

    @Column
    private String codNotifCumul;

    @Column
    private Long idModulNotifCumul;

    @Column
    private String htmlTableHeader;

    @OneToMany(mappedBy = "cod", cascade = {CascadeType.ALL})
    private List<BpmConfigNotificareOrar> orare;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Long getIdModul() {
        return idModul;
    }

    public void setIdModul(Long idModul) {
        this.idModul = idModul;
    }

    public String getStrTip() {
        return strTip;
    }

    public void setStrTip(String strTip) {
        this.strTip = strTip;
    }

    public String getAditionalToListaId() {
        return aditionalToListaId;
    }

    public void setAditionalToListaId(String aditionalToListaId) {
        this.aditionalToListaId = aditionalToListaId;
    }

    public String getAditionalToListaEmail() {
        return aditionalToListaEmail;
    }

    public void setAditionalToListaEmail(String aditionalToListaEmail) {
        this.aditionalToListaEmail = aditionalToListaEmail;
    }

    public String getAditionalCcListaId() {
        return aditionalCcListaId;
    }

    public void setAditionalCcListaId(String aditionalCcListaId) {
        this.aditionalCcListaId = aditionalCcListaId;
    }

    public String getAditionalCcListaEmail() {
        return aditionalCcListaEmail;
    }

    public void setAditionalCcListaEmail(String aditionalCcListaEmail) {
        this.aditionalCcListaEmail = aditionalCcListaEmail;
    }

    public String getExclusToListaId() {
        return exclusToListaId;
    }

    public void setExclusToListaId(String exclusToListaId) {
        this.exclusToListaId = exclusToListaId;
    }

    public String getExclusToListaEmail() {
        return exclusToListaEmail;
    }

    public void setExclusToListaEmail(String exclusToListaEmail) {
        this.exclusToListaEmail = exclusToListaEmail;
    }

    public String getExclusCcListaId() {
        return exclusCcListaId;
    }

    public void setExclusCcListaId(String exclusCcListaId) {
        this.exclusCcListaId = exclusCcListaId;
    }

    public String getExclusCcListaEmail() {
        return exclusCcListaEmail;
    }

    public void setExclusCcListaEmail(String exclusCcListaEmail) {
        this.exclusCcListaEmail = exclusCcListaEmail;
    }

    public String getSubiect() {
        return subiect;
    }

    public void setSubiect(String subiect) {
        this.subiect = subiect;
    }

    public String getAntet() {
        return antet;
    }

    public void setAntet(String antet) {
        this.antet = antet;
    }

    public String getContinut() {
        return continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }

    public String getSubsol() {
        return subsol;
    }

    public void setSubsol(String subsol) {
        this.subsol = subsol;
    }

    public Long getNrCicluri() {
        return nrCicluri;
    }

    public void setNrCicluri(Long nrCicluri) {
        this.nrCicluri = nrCicluri;
    }

    public Boolean getAreOrar() {
        return areOrar;
    }

    public void setAreOrar(Boolean areOrar) {
        this.areOrar = areOrar;
    }

    public String getCodNotifCumul() {
        return codNotifCumul;
    }

    public void setCodNotifCumul(String codNotifCumul) {
        this.codNotifCumul = codNotifCumul;
    }

    public Long getIdModulNotifCumul() {
        return idModulNotifCumul;
    }

    public void setIdModulNotifCumul(Long idModulNotifCumul) {
        this.idModulNotifCumul = idModulNotifCumul;
    }

    public String getHtmlTableHeader() {
        return htmlTableHeader;
    }

    public void setHtmlTableHeader(String htmlTableHeader) {
        this.htmlTableHeader = htmlTableHeader;
    }

    public List<BpmConfigNotificareOrar> getOrare() {
        return orare;
    }

    public void setOrare(List<BpmConfigNotificareOrar> orare) {
        this.orare = orare;
    }
}
