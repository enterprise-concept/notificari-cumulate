package com.ec.notificariCumulate.persistence.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "app_notificare_trimisa")
public class AppNotificareTrimisa {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "`to`")
    private String to;

    @Column
    private String cc;

    @Column
    private String bcc;

    @Column
    private String subiect;

    @Column
    private String continut;

    @Column
    private LocalDateTime dataTrimitere;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getSubiect() {
        return subiect;
    }

    public void setSubiect(String subiect) {
        this.subiect = subiect;
    }

    public String getContinut() {
        return continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }

    public LocalDateTime getDataTrimitere() {
        return dataTrimitere;
    }

    public void setDataTrimitere(LocalDateTime dataTrimitere) {
        this.dataTrimitere = dataTrimitere;
    }
}
