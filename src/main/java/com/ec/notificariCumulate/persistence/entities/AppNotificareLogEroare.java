package com.ec.notificariCumulate.persistence.entities;

import javax.persistence.*;

@Entity
@Table(name = "app_notificare_log_eroare")
public class AppNotificareLogEroare {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String codNotificare;

    @Column
    private String mesaj;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodNotificare() {
        return codNotificare;
    }

    public void setCodNotificare(String codNotificare) {
        this.codNotificare = codNotificare;
    }

    public String getMesaj() {
        return mesaj;
    }

    public void setMesaj(String mesaj) {
        this.mesaj = mesaj;
    }
}
