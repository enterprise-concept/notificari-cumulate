package com.ec.notificariCumulate.persistence.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "app_notificare")
public class AppNotificare {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String cod;

    @Column
    private Long idModul;

    @Column
    private String listaTo;

    @Column
    private String listaCc;

    @Column
    private String listaBcc;

    @Column
    private String subiect;

    @Column
    private String continut;

    @Column
    private LocalDateTime dataGenerare;

    @Column
    private LocalDateTime dataScadenta;

    @Column
    private LocalDateTime dataTransmisie;

    @Column
    private String strStare;

    @Column
    private Long idObiect;

    @Column
    private String antet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Long getIdModul() {
        return idModul;
    }

    public void setIdModul(Long idModul) {
        this.idModul = idModul;
    }

    public String getListaTo() {
        return listaTo;
    }

    public void setListaTo(String listaTo) {
        this.listaTo = listaTo;
    }

    public String getListaCc() {
        return listaCc;
    }

    public void setListaCc(String listaCc) {
        this.listaCc = listaCc;
    }

    public String getListaBcc() {
        return listaBcc;
    }

    public void setListaBcc(String listaBcc) {
        this.listaBcc = listaBcc;
    }

    public String getSubiect() {
        return subiect;
    }

    public void setSubiect(String subiect) {
        this.subiect = subiect;
    }

    public String getContinut() {
        return continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }

    public LocalDateTime getDataGenerare() {
        return dataGenerare;
    }

    public void setDataGenerare(LocalDateTime dataGenerare) {
        this.dataGenerare = dataGenerare;
    }

    public LocalDateTime getDataScadenta() {
        return dataScadenta;
    }

    public void setDataScadenta(LocalDateTime dataScadenta) {
        this.dataScadenta = dataScadenta;
    }

    public LocalDateTime getDataTransmisie() {
        return dataTransmisie;
    }

    public void setDataTransmisie(LocalDateTime dataTransmisie) {
        this.dataTransmisie = dataTransmisie;
    }

    public String getStrStare() {
        return strStare;
    }

    public void setStrStare(String strStare) {
        this.strStare = strStare;
    }

    public Long getIdObiect() {
        return idObiect;
    }

    public void setIdObiect(Long idObiect) {
        this.idObiect = idObiect;
    }

    public String getAntet() {
        return antet;
    }

    public void setAntet(String antet) {
        this.antet = antet;
    }

}
