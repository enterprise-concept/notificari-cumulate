package com.ec.notificariCumulate.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "vw_lista_cereri_in_asteptare_data_start_depasita")
public class VwListaCereriInAsteptareDataStartDepasita {

    @Id
    @Column(name = "rowId")
    private Long rowId;

    @Column(name = "nrCerere")
    private String nrCerere;

    @Column(name = "dataSolicitare")
    private LocalDate dataSolicitare;

    @Column(name = "solicitant")
    private String solicitant;

    @Column(name = "furnizor")
    private String furnizor;

    @Column(name = "email")
    private String email;

    @Column(name = "dataStart")
    private LocalDate dataStart;

    @Column(name = "nrZile")
    private Long nrZile;

    @Column(name = "idSolicitare")
    private Long idSolicitare;

    public Long getRowId() {
        return rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public String getNrCerere() {
        return nrCerere;
    }

    public void setNrCerere(String nrCerere) {
        this.nrCerere = nrCerere;
    }

    public LocalDate getDataSolicitare() {
        return dataSolicitare;
    }

    public void setDataSolicitare(LocalDate dataSolicitare) {
        this.dataSolicitare = dataSolicitare;
    }

    public String getSolicitant() {
        return solicitant;
    }

    public void setSolicitant(String solicitant) {
        this.solicitant = solicitant;
    }

    public String getFurnizor() {
        return furnizor;
    }

    public void setFurnizor(String furnizor) {
        this.furnizor = furnizor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataStart() {
        return dataStart;
    }

    public void setDataStart(LocalDate dataStart) {
        this.dataStart = dataStart;
    }

    public Long getNrZile() {
        return nrZile;
    }

    public void setNrZile(Long nrZile) {
        this.nrZile = nrZile;
    }

    public Long getIdSolicitare() {
        return idSolicitare;
    }

    public void setIdSolicitare(Long idSolicitare) {
        this.idSolicitare = idSolicitare;
    }
}
