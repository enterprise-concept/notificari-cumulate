package com.ec.notificariCumulate.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bpm_sablon_notificare")
public class BpmSablonNotificare {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "cod")
    private String cod;

    @Column(name = "subject")
    private String subject;

    @Column(name = "header")
    private String header;

    @Column(name = "body")
    private String body;

    @Column(name = "footer")
    private String footer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
}
