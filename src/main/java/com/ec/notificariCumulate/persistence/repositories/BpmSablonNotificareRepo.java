package com.ec.notificariCumulate.persistence.repositories;

import com.ec.notificariCumulate.persistence.entities.BpmSablonNotificare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BpmSablonNotificareRepo extends JpaRepository<BpmSablonNotificare, Long> {

    public BpmSablonNotificare findByCod(String cod);

}
