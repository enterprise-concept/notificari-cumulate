package com.ec.notificariCumulate.persistence.repositories;

import com.ec.notificariCumulate.persistence.entities.VwListaCereriInAsteptareDataStartPrag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VwListaCereriInAsteptareDataStartPragRepo extends JpaRepository<VwListaCereriInAsteptareDataStartPrag, Long> {

}
