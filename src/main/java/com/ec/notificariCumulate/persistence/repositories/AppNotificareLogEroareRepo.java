package com.ec.notificariCumulate.persistence.repositories;

import com.ec.notificariCumulate.persistence.entities.AppNotificareLogEroare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppNotificareLogEroareRepo extends JpaRepository<AppNotificareLogEroare, Long> {
}
