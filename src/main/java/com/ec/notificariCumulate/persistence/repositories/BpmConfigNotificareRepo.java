package com.ec.notificariCumulate.persistence.repositories;

import com.ec.notificariCumulate.persistence.entities.BpmConfigNotificare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BpmConfigNotificareRepo extends JpaRepository<BpmConfigNotificare, String>{

    @Query(value = "SELECT b FROM BpmConfigNotificare b JOIN FETCH b.orare b1 WHERE b1.zi = :zi AND b1.ora = :ora AND b.strTip = 'C' AND b.idModul = b1.idModul")
    public List<BpmConfigNotificare> findAllByZiAndOra(int zi, int ora);

}
