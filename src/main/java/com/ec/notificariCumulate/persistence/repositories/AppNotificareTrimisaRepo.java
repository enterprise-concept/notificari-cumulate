package com.ec.notificariCumulate.persistence.repositories;

import com.ec.notificariCumulate.persistence.entities.AppNotificareTrimisa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppNotificareTrimisaRepo extends JpaRepository<AppNotificareTrimisa, Long> {
}
