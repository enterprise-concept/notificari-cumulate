package com.ec.notificariCumulate.persistence.repositories;

import com.ec.notificariCumulate.persistence.entities.VwListaCereriInAsteptareDataStartDepasita;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VwListaCereriInAsteptareDataStartDepasitaRepo extends JpaRepository<VwListaCereriInAsteptareDataStartDepasita, Long> {
}
