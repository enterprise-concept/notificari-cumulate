package com.ec.notificariCumulate.persistence.repositories;

import com.ec.notificariCumulate.persistence.entities.AppNotificare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AppNotificareRepo extends JpaRepository<AppNotificare, Long> {

    @Query(value = "SELECT * FROM app_notificare WITH (NOLOCK) WHERE cod IN :coduri AND dataGenerare > GETDATE() - 40 AND strStare = 'NP'", nativeQuery = true)
    public List<AppNotificare> findAllByCodAndDataGenerare(List<String> coduri);

}
