package com.ec.notificariCumulate.mappers;

import com.ec.notificariCumulate.dto.AppNotificareDto;
import com.ec.notificariCumulate.persistence.entities.AppNotificare;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class AppNotificareMapper {

    public AppNotificareDto convertEntityToDto(AppNotificare notificare) {
        AppNotificareDto dto = new AppNotificareDto();
        BeanUtils.copyProperties(notificare, dto);
        return dto;
    }

    public List<AppNotificareDto> convertEntityToDto(List<AppNotificare> notificari) {
        List<AppNotificareDto> dtos = new ArrayList<>();
        notificari.forEach(notificare -> {
            AppNotificareDto dto = new AppNotificareDto();
            BeanUtils.copyProperties(notificare, dto);
            dtos.add(dto);
        });
        return dtos;
    }

    public AppNotificare convertDtoToEntity(AppNotificareDto dto) {
        AppNotificare notificare = new AppNotificare();
        BeanUtils.copyProperties(dto, notificare);
        return notificare;
    }

    public List<AppNotificare> convertDtoToEntity(Set<AppNotificareDto> dtos) {
        List<AppNotificare> notificari = new ArrayList<>();
        dtos.forEach(dto -> {
            AppNotificare notificare = new AppNotificare();
            BeanUtils.copyProperties(dto, notificare);
            notificari.add(notificare);
        });
        return notificari;
    }

}
