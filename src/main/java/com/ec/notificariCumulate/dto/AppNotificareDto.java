package com.ec.notificariCumulate.dto;

import java.time.LocalDateTime;

public class AppNotificareDto {

    private Long id;
    private String cod;
    private Long idModul;
    private String listaTo;
    private String listaCc;
    private String listaBcc;
    private String subiect;
    private String continut;
    private LocalDateTime dataGenerare;
    private LocalDateTime dataScadenta;
    private LocalDateTime dataTransmisie;
    private String strStare;
    private Long idObiect;
    private String antet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Long getIdModul() {
        return idModul;
    }

    public void setIdModul(Long idModul) {
        this.idModul = idModul;
    }

    public String getListaTo() {
        return listaTo;
    }

    public void setListaTo(String listaTo) {
        this.listaTo = listaTo;
    }

    public String getListaCc() {
        return listaCc;
    }

    public void setListaCc(String listaCc) {
        this.listaCc = listaCc;
    }

    public String getListaBcc() {
        return listaBcc;
    }

    public void setListaBcc(String listaBcc) {
        this.listaBcc = listaBcc;
    }

    public String getSubiect() {
        return subiect;
    }

    public void setSubiect(String subiect) {
        this.subiect = subiect;
    }

    public String getContinut() {
        return continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }

    public LocalDateTime getDataGenerare() {
        return dataGenerare;
    }

    public void setDataGenerare(LocalDateTime dataGenerare) {
        this.dataGenerare = dataGenerare;
    }

    public LocalDateTime getDataScadenta() {
        return dataScadenta;
    }

    public void setDataScadenta(LocalDateTime dataScadenta) {
        this.dataScadenta = dataScadenta;
    }

    public LocalDateTime getDataTransmisie() {
        return dataTransmisie;
    }

    public void setDataTransmisie(LocalDateTime dataTransmisie) {
        this.dataTransmisie = dataTransmisie;
    }

    public String getStrStare() {
        return strStare;
    }

    public void setStrStare(String strStare) {
        this.strStare = strStare;
    }

    public Long getIdObiect() {
        return idObiect;
    }

    public void setIdObiect(Long idObiect) {
        this.idObiect = idObiect;
    }

    public String getAntet() {
        return antet;
    }

    public void setAntet(String antet) {
        this.antet = antet;
    }
}
