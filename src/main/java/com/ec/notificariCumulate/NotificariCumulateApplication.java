package com.ec.notificariCumulate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificariCumulateApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotificariCumulateApplication.class, args);
	}

}
