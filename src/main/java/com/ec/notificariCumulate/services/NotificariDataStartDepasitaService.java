package com.ec.notificariCumulate.services;

import com.ec.notificariCumulate.persistence.entities.AppNotificare;
import com.ec.notificariCumulate.persistence.entities.BpmSablonNotificare;
import com.ec.notificariCumulate.persistence.entities.VwListaCereriInAsteptareDataStartDepasita;
import com.ec.notificariCumulate.persistence.repositories.AppNotificareRepo;
import com.ec.notificariCumulate.persistence.repositories.BpmSablonNotificareRepo;
import com.ec.notificariCumulate.persistence.repositories.VwListaCereriInAsteptareDataStartDepasitaRepo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@EnableScheduling
public class NotificariDataStartDepasitaService {

    @Value("${app-config.liferay.url.site}")
    private String urlSite;

    @Value("${app-config.ul.project.id}")
    private String projId;

    @Value("${app-config.liferay.url.site}")
    private String urlLiferaySite;

    @Value("${smtp.sender.email}")
    private String smtpSenderEmail;

    @Value("${smtp.sender.name}")
    private String smtpSenderName;

    @Value("${smtp.server.name}")
    private String smtpServerName;

    @Value("${smtp.server.port}")
    private String smtpServerPort;

    @Value("${smtp.server.user}")
    private String smtpServerUser;

    @Value("${smtp.server.pass}")
    private String smtpServerPassword;

    @Value("${test-mode.active}")
    private boolean testModeActive;

    @Value("${test-mode.smtp.receipients.to}")
    private String testModeTo;

    @Value("${test-mode.smtp.receipients.cc}")
    private String testModeCc;

    private final VwListaCereriInAsteptareDataStartDepasitaRepo vwListaCereriInAsteptareDataStartDepasitaRepo;
    private final BpmSablonNotificareRepo bpmSablonNotificareRepo;
    private final AppNotificareRepo appNotificareRepo;

    public NotificariDataStartDepasitaService(VwListaCereriInAsteptareDataStartDepasitaRepo vwListaCereriInAsteptareDataStartDepasitaRepo,
                                              BpmSablonNotificareRepo bpmSablonNotificareRepo,
                                              AppNotificareRepo appNotificareRepo) {
        this.vwListaCereriInAsteptareDataStartDepasitaRepo = vwListaCereriInAsteptareDataStartDepasitaRepo;
        this.bpmSablonNotificareRepo = bpmSablonNotificareRepo;
        this.appNotificareRepo = appNotificareRepo;
    }

    @Scheduled(cron = "0 40 7 * * *")
    public void sendNotification() {
        try{
            System.out.println("*************START NOTIFICARI DATA START DEPASITA ***********" + LocalDateTime.now());
            List<VwListaCereriInAsteptareDataStartDepasita> lines = vwListaCereriInAsteptareDataStartDepasitaRepo.findAll();

            Set<String> emails = new HashSet<>();
            if(lines != null && lines.size() > 0) {
                for(VwListaCereriInAsteptareDataStartDepasita line : lines) {
                    if(line.getEmail() != null) {
                        emails.add(line.getEmail());
                    }
                }

                for(String email: emails) {
                    BpmSablonNotificare bpmSablonNotificare = bpmSablonNotificareRepo.findByCod("VALIDARE_CERERE_AJUSTARE_DATA_START_DEPASITA_V2");
                    List<VwListaCereriInAsteptareDataStartDepasita> linesEmail = lines.stream().filter(l -> l.getEmail().equals(email)).collect(Collectors.toList());
                    String lineText = StringUtils.substringBetween(bpmSablonNotificare.getBody(), "<!--{each datas}-->", "<!--{end each}-->");
                    String continut = "";
                    for(VwListaCereriInAsteptareDataStartDepasita line : linesEmail) {
                        String nrCerere = line.getNrCerere() == null ? "" : line.getNrCerere();
                        LocalDate dataSolicitare = line.getDataSolicitare();
                        String solicitant = line.getSolicitant() == null ? "" : line.getSolicitant();
                        String furnizor = line.getFurnizor() == null ? "" : line.getFurnizor();
                        LocalDate dataStart = line.getDataStart();
                        String dataSolicitareString = "";
                        String dataStartString = "";
                        if(dataSolicitare != null) {
                            dataSolicitareString = dataSolicitare.toString();
                        }
                        if(dataStart != null) {
                            dataStartString = dataStart.toString();
                        }

                        continut += lineText.replace("${nr_cerere}",nrCerere)
                                .replace("${data_solicitare}", dataSolicitareString)
                                .replace("${solicitant}", solicitant)
                                .replace("${furnizor}", furnizor)
                                .replace("${dataStart}", dataStartString);
                    }
                    String text = bpmSablonNotificare.getBody().replace(lineText, continut);
                    text += bpmSablonNotificare.getFooter();
                    text = text.replace("$$link$$",getUrl("/taskuri-active"));

                    sendEmail(text, bpmSablonNotificare.getSubject(),null, email);
                    buildAppNotificare(email,text,bpmSablonNotificare.getSubject());


                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println("Eroare la transmiterea notificarilor cu data start depasita! Mesaj eroare: " + e.getMessage());
        }
        System.out.println("*************END NOTIFICARI DATA START DEPASITA ***********" + LocalDateTime.now());
    }

    private void buildAppNotificare(String email, String continut, String subiect) {
        AppNotificare appNotificare = new AppNotificare();
        appNotificare.setCod("VALIDARE_CERERE_AJUSTARE_DATA_START_DEPASITA_V2");
        appNotificare.setStrStare("T");
        appNotificare.setIdModul(2L);
        appNotificare.setDataGenerare(LocalDateTime.now());
        appNotificare.setDataTransmisie(LocalDateTime.now());
        appNotificare.setListaTo(email);
        appNotificare.setSubiect(subiect);
        appNotificare.setContinut(continut);
        appNotificareRepo.saveAndFlush(appNotificare);
    }


    private String getUrl(String pageLink) {
        String siteUrl = urlSite.concat("?p=").concat(projId).concat("&tip=1&url=")
                .concat(urlLiferaySite);
        return "<a href=" + siteUrl + pageLink + ">aici</a>";
    }

    private void sendEmail(String message, String subiect, List<String> pathFile, String toEmail) throws UnsupportedEncodingException, MessagingException {
        Properties props = new Properties();
        if(this.testModeActive) {
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
        }
        props.put("mail.debug", "false");
        props.put("mail.smtp.timeout", 1000);
        props.put("mail.smtp.host", this.smtpServerName);
        props.put("mail.smtp.port", this.smtpServerPort);

        props.put("mail.smtp.ssl.trust", this.smtpServerName);
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");

        Session session;

        if(this.testModeActive) {
            session = Session.getInstance(props,
                    new Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(smtpServerUser, smtpServerPassword);
                        }
                    });
        } else {
            session = Session.getInstance(props);
        }

        Message emailMessage = new MimeMessage(session);
        emailMessage.setFrom(new InternetAddress(smtpSenderEmail, smtpSenderName));
        if(testModeActive) {
            emailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(testModeTo));
            emailMessage.setRecipients(Message.RecipientType.CC, InternetAddress.parse(testModeCc));
        } else {
            emailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
        }
        emailMessage.setSubject(subiect);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        String fileName = "";
        if(pathFile != null) {
            for(int i=0; i<pathFile.size(); i++){
                messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(pathFile.get(i));
                messageBodyPart.setDataHandler(new DataHandler(source));
                fileName = pathFile.get(i);
                messageBodyPart.setFileName(fileName.replace("\\", "/").substring(fileName.lastIndexOf("/")+1));
                multipart.addBodyPart(messageBodyPart);
            }
        }
        emailMessage.setContent( multipart );
        Transport.send(emailMessage);
    }

}
