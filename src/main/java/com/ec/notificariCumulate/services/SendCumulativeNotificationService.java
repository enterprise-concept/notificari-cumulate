package com.ec.notificariCumulate.services;

import com.ec.notificariCumulate.dto.AppNotificareDto;
import com.ec.notificariCumulate.mappers.AppNotificareMapper;
import com.ec.notificariCumulate.persistence.entities.*;
import com.ec.notificariCumulate.persistence.repositories.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.*;

@Service
@EnableScheduling
public class SendCumulativeNotificationService {

    private final BpmConfigNotificareRepo bpmConfigNotificareRepo;
    private final AppNotificareRepo appNotificareRepo;
    private final AppNotificareTrimisaRepo appNotificareTrimisaRepo;
    private final AppNotificareLogEroareRepo appNotificareLogEroareRepo;
    private final AppNotificareMapper appNotificareMapper;

    @Value("${smtp.sender.email}")
    private String smtpSenderEmail;

    @Value("${smtp.sender.name}")
    private String smtpSenderName;

    @Value("${smtp.server.name}")
    private String smtpServerName;

    @Value("${smtp.server.port}")
    private String smtpServerPort;

    @Value("${smtp.server.user}")
    private String smtpServerUser;

    @Value("${smtp.server.pass}")
    private String smtpServerPassword;

    @Value("${test-mode.active}")
    private boolean testModeActive;

    @Value("${test-mode.smtp.receipients.to}")
    private String testModeTo;

    @Value("${test-mode.smtp.receipients.cc}")
    private String testModeCc;

    @Value("${fs.config.root}")
    private String fsConfigRoot;

    @Value("${smtp.mass.subject}")
    private String smtpMassSubject;

    @Value("${smtp.mass.body}")
    private String smtpMassBody;

    public SendCumulativeNotificationService(BpmConfigNotificareRepo bpmConfigNotificareRepo,
                                             AppNotificareRepo appNotificareRepo,
                                             AppNotificareTrimisaRepo appNotificareTrimisaRepo,
                                             AppNotificareLogEroareRepo appNotificareLogEroareRepo,
                                             AppNotificareMapper appNotificareMapper) {
        this.bpmConfigNotificareRepo = bpmConfigNotificareRepo;
        this.appNotificareRepo = appNotificareRepo;
        this.appNotificareTrimisaRepo = appNotificareTrimisaRepo;
        this.appNotificareLogEroareRepo = appNotificareLogEroareRepo;
        this.appNotificareMapper = appNotificareMapper;
    }

    @Scheduled(cron = "0 0 */1 * * *")
    public String sendNotifications() {
        try{
            System.out.println("Start send notifications");
            Date date = new Date();
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(date);
            int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
            int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;

            List<BpmConfigNotificare> sabloaneNotificari = bpmConfigNotificareRepo.findAllByZiAndOra(day,currentHour);

            if(sabloaneNotificari.size() > 0) {
                List<String> coduriNotificare = new ArrayList<>();
                Set<String> coduriUnice = new HashSet<>();
                sabloaneNotificari.forEach(sablon -> {
                    if(coduriUnice.add(sablon.getCod())) {
                        coduriNotificare.add(sablon.getCod());
                    }
                });

                List<AppNotificare> notificari = appNotificareRepo.findAllByCodAndDataGenerare(coduriNotificare);

                List<AppNotificareDto> notificariDto = appNotificareMapper.convertEntityToDto(notificari);

                Set<AppNotificareDto> notificariProcesate = new HashSet<>();

                Set<String> emails = new HashSet<>();
                notificariDto.forEach(n -> {
                    emails.add(n.getListaTo());
                });

                for(String email: emails) {
                    List<String> allFilePaths = new ArrayList<String>();

                    Set<String> coduriNotificari = new HashSet<>();
                    notificariDto.forEach(n -> {
                        if(n.getListaTo().equalsIgnoreCase(email)) {
                            coduriNotificari.add(n.getCod());
                        }
                    });

                    for(String codNotificare: coduriNotificari) {
                        List<AppNotificareDto> notificariGrupate = new ArrayList<>();
                        notificariDto.forEach(n -> {
                            if(n.getListaTo().equalsIgnoreCase(email) && n.getCod().equalsIgnoreCase(codNotificare)) {
                                notificariGrupate.add(n);
                            }
                        });

                        if(notificariGrupate.size() > 0) {
                            String htmlTableHeader = "";
                            String subsol = "";
                            for(BpmConfigNotificare s : sabloaneNotificari) {
                                if(s.getCod().equalsIgnoreCase(codNotificare)) {
                                    htmlTableHeader = s.getHtmlTableHeader();
                                    subsol = s.getSubsol();
                                }
                            }

                            String messageBody = "<html>";
                            messageBody += "<head>";
                            messageBody += "</head>";
                            messageBody += "<body style='padding: 2%;'>";
                            messageBody += notificariGrupate.get(0).getAntet();
                            messageBody += htmlTableHeader;

                            for(AppNotificareDto notificareDto: notificariGrupate) {
                                if(notificareDto.getContinut() != null) {
                                    messageBody += notificareDto.getContinut();
                                    notificariProcesate.add(notificareDto);
                                }else {
                                    insereazaEroare(codNotificare, "Nu exista date pe coloana continut in tabela app_notificare pentru notificarea cu id-ul: " + notificareDto.getId() + " !");
                                }
                            }

                            messageBody += "</table>";
                            messageBody += "<br/><br/>";
                            messageBody += subsol;
                            String numeFisier = fsConfigRoot + notificariGrupate.get(0).getSubiect() + "_" + calendar.get(Calendar.YEAR) + "-" +  calendar.get(Calendar.MONTH) + "-" +  calendar.get(Calendar.DAY_OF_MONTH) + "-" +  calendar.get(Calendar.HOUR) + "-" +  calendar.get(Calendar.MINUTE) + ".html";
                            PrintWriter writer = new PrintWriter(numeFisier);
                            messageBody = messageBody.replace("<table>", "<table style='border: 1px solid black'>");
                            messageBody = messageBody.replace("<tr>", "<tr style='border: 1px solid black'>");
                            messageBody = messageBody.replace("<th>", "<th style='border: 1px solid black'>");
                            messageBody = messageBody.replace("<td>", "<td style='border: 1px solid black'>");
                            messageBody += "</body></html>";
                            writer.print(messageBody);
                            writer.close();
                            allFilePaths.add(numeFisier);

                        }

                    }

                    String listaAtasament = "<ul>";
                    if(allFilePaths != null && allFilePaths.size() > 0) {
                        for(int j = 0 ; j < allFilePaths.size(); j++) {
                            listaAtasament += "<li>" + allFilePaths.get(j).substring(allFilePaths.get(j).lastIndexOf("/") + 1, allFilePaths.get(j).length()) + "</li>";
                        }
                        listaAtasament += "</ul>";
                        String smtpMassBodyClone = smtpMassBody;
                        smtpMassBodyClone = smtpMassBodyClone.replace("##lista##", listaAtasament);
                        try {
                            sendEmail(smtpMassBodyClone, smtpMassSubject, allFilePaths, email);
                            marcheazaNotificariProcesate(notificariProcesate);
                            insereazaNotificareTrimisa(smtpMassBodyClone, smtpMassSubject, email);
                            System.out.println("Send notifications...");
                        } catch (MessagingException e) {
                            System.out.println("Aceasta eroare nu a oprit sistemul de notificari, doar a fost captata!");
                            e.printStackTrace();
                        }
                    }
                }

            }else {
                System.out.println("Nothing to send...");
            }

        }catch (Exception e) {
            e.printStackTrace();
            return "Eroare la transmiterea notificarilor! Mesaj eroare: " + e.getMessage();
        }
        System.out.println("Notifications sends with succes!");
        return "Succes la transmiterea notificarilor";
    }

    private void sendEmail(String message, String subiect, List<String> pathFile, String toEmail) throws UnsupportedEncodingException, MessagingException {
        Properties props = new Properties();
        if(this.testModeActive) {
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
        }
        props.put("mail.debug", "false");
        props.put("mail.smtp.timeout", 1000);
        props.put("mail.smtp.host", this.smtpServerName);
        props.put("mail.smtp.port", this.smtpServerPort);

        props.put("mail.smtp.ssl.trust", this.smtpServerName);
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");

        Session session;

        if(this.testModeActive) {
            session = Session.getInstance(props,
                    new Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(smtpServerUser, smtpServerPassword);
                        }
                    });
        } else {
            session = Session.getInstance(props);
        }

        Message emailMessage = new MimeMessage(session);
        emailMessage.setFrom(new InternetAddress(smtpSenderEmail, smtpSenderName));
        if(testModeActive) {
            emailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(testModeTo));
            emailMessage.setRecipients(Message.RecipientType.CC, InternetAddress.parse(testModeCc));
        } else {
            emailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
        }
        emailMessage.setSubject(subiect);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        String fileName = "";
        for(int i=0; i<pathFile.size(); i++){
            messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(pathFile.get(i));
            messageBodyPart.setDataHandler(new DataHandler(source));
            fileName = pathFile.get(i);
            messageBodyPart.setFileName(fileName.replace("\\", "/").substring(fileName.lastIndexOf("/")+1));
            multipart.addBodyPart(messageBodyPart);
        }
        emailMessage.setContent( multipart );
        Transport.send(emailMessage);
    }

    private Long insereazaNotificareTrimisa(String mesaj, String subiect, String to) {
        AppNotificareTrimisa appNotificareTrimisa = new AppNotificareTrimisa();
        if(to != null && to.length() > 2000) {
            appNotificareTrimisa.setTo(to.substring(0,1999));
        }else {
            appNotificareTrimisa.setTo(to);
        }
        if(subiect != null && subiect.length() > 250) {
            appNotificareTrimisa.setSubiect(subiect.substring(0,249));
        }else {
            appNotificareTrimisa.setSubiect(subiect);
        }

        if(mesaj != null && mesaj.length() > 5000) {
            appNotificareTrimisa.setContinut(mesaj.substring(0,4999));
        }else {
            appNotificareTrimisa.setContinut(mesaj);
        }

        appNotificareTrimisa.setDataTrimitere(LocalDateTime.now());
        this.appNotificareTrimisaRepo.saveAndFlush(appNotificareTrimisa);
        return appNotificareTrimisa.getId();
    }

    private void marcheazaNotificariProcesate(Set<AppNotificareDto> list) {
        for(AppNotificareDto n : list) {
            n.setStrStare("P");
        }
        List<AppNotificare> notificari = appNotificareMapper.convertDtoToEntity(list);
        this.appNotificareRepo.saveAllAndFlush(notificari);
    }

    private Long insereazaEroare(String codNotificare, String mesaj) {
        AppNotificareLogEroare appNotificareLogEroare = new AppNotificareLogEroare();
        appNotificareLogEroare.setCodNotificare(codNotificare);
        appNotificareLogEroare.setMesaj(mesaj);
        this.appNotificareLogEroareRepo.saveAndFlush(appNotificareLogEroare);
        return appNotificareLogEroare.getId();
    }


}
