package com.ec.notificariCumulate.controllers;

import com.ec.notificariCumulate.services.NotificariDataStartDepasitaService;
import com.ec.notificariCumulate.services.NotificariDataStartPragService;
import com.ec.notificariCumulate.services.SendCumulativeNotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("send-notification")
public class SendNotificationManual {

    private final SendCumulativeNotificationService sendCumulativeNotificationService;
    private final NotificariDataStartDepasitaService notificariDataStartDepasitaService;
    private final NotificariDataStartPragService notificariDataStartPragService;

    public SendNotificationManual(SendCumulativeNotificationService sendCumulativeNotificationService,
                                  NotificariDataStartDepasitaService notificariDataStartDepasitaService,
                                  NotificariDataStartPragService notificariDataStartPragService) {
        this.sendCumulativeNotificationService = sendCumulativeNotificationService;
        this.notificariDataStartDepasitaService = notificariDataStartDepasitaService;
        this.notificariDataStartPragService = notificariDataStartPragService;
    }

    @PostMapping
    public ResponseEntity<String> sendNotification() {
        return ResponseEntity.ok(sendCumulativeNotificationService.sendNotifications());
    }

    @GetMapping("/data-start-depasita")
    public void sendDataStartDepasitaNotification() {
        notificariDataStartDepasitaService.sendNotification();
    }

    @GetMapping("/data-prag")
    public void sendDataPragNotification() {
        notificariDataStartPragService.sendNotification();
    }

    @PostMapping("/test")
    public ResponseEntity<String> test() {
        System.out.println("Test efectuat cu succes!");
        return ResponseEntity.ok("test-ok");
    }


}
